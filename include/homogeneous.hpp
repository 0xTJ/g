#ifndef INCLUDE_HOMOGENEOUS_HPP
#define INCLUDE_HOMOGENEOUS_HPP

#include "M.hpp"
#include <cmath>
#include <cassert>

namespace Homogeneous {
    using BaseType = double;

    template<std::size_t N>
    using RawPoint = M::ColVector<BaseType, N>;

    template<std::size_t N>
    using Coord = M::ColVector<BaseType, N + 1>;

    template<std::size_t N>
    using Transform = M::Matrix<BaseType, N + 1, N + 1>;

    template<std::size_t N>
    using Projection = M::Matrix<BaseType, N, N + 1>;

    using RawPoint2D = RawPoint<2>;
    using RawPoint3D = RawPoint<3>;

    using Coord2D = Coord<2>;
    using Coord3D = Coord<3>;
    
    using Transform2D = Transform<2>;
    using Transform3D = Transform<3>;

    using Projection2D = Projection<2>;
    using Projection3D = Projection<3>;

    using Plane3D = M::RowVector<BaseType, 4>;

    template<std::size_t N>
    constexpr inline auto raw_point_null() {
        RawPoint<N> result{};
        return result;
    }

    template<std::size_t N>
    constexpr inline auto point_null() {
        Coord<N> result{};
        result[N] = 1;
        return result;
    }

    template<std::size_t N>
    constexpr inline auto vect_null() {
        Coord<N> result{};
        return result;
    }

    template<std::size_t N>
    constexpr inline auto transform_null() {
        Transform<N> result{};
        for (std::size_t i = 0; i <= N; ++i) {
            result[i][i] = 1;
        }
        return result;
    }

    // template<std::size_t N>
    // constexpr inline auto projection_null() {
    //     Projection<N> result{};
    //     for (std::size_t i = 0; i < N; ++i) {
    //         result[i][i] = 1;
    //     }
    //     return result;
    // }

    constexpr inline auto plane3d_null() {
        Plane3D result{};
        return result;
    }

    template<std::size_t N>
    constexpr inline bool is_point(const Coord<N> &coord) {
        return coord[N] == 1;
    }

    template<std::size_t N>
    constexpr inline bool is_vector(const Coord<N> &coord) {
        return coord[N] == 0;
    }

    template<std::size_t N>
    constexpr inline auto point_from_raw(RawPoint<N> point) {
        Coord<N> result = point_null<N>();
        for (std::size_t i = 0; i < N; ++i) {
            result[i] = point[i];
        }
        return result;
    }

    template<std::size_t N>
    constexpr inline auto point_to_raw(Coord<N> coord) {
        RawPoint<N> result{};
        for (std::size_t i = 0; i < N; ++i) {
            result[i] = coord[i] / coord[N];
        }
        return result;
    }

    template<std::size_t N>
    constexpr inline auto vect_from_raw(RawPoint<N> point) {
        Coord<N> result = vect_null<N>();
        for (std::size_t i = 0; i < N; ++i) {
            result[i] = point[i];
        }
        return result;
    }

    template<std::size_t N>
    constexpr inline auto vect_to_raw(Coord<N> coord) {
        assert(coord[N] == 0);
        RawPoint<N> result{};
        for (std::size_t i = 0; i < N; ++i) {
            result[i] = coord[i];
        }
        return result;
    }

    inline auto rotation_2d(BaseType theta) {
        Transform2D result = transform_null<2>();

        auto c_theta = std::cos(theta);
        auto s_theta = std::sin(theta);

        result[0][0] = c_theta;
        result[0][1] = -s_theta;
        result[1][0] = s_theta;
        result[1][1] = c_theta;

        return result;
    }

    inline auto rotation_inverse_2d(BaseType theta) {
        return rotation_2d(-theta);
    }

    inline auto rotation_3d_x(BaseType theta) {
        auto c_theta = std::cos(theta);
        auto s_theta = std::sin(theta);

        Transform3D result{{{
            { 1, 0, 0, 0 },
            { 0, c_theta, -s_theta, 0 },
            { 0, s_theta, c_theta, 0 },
            { 0, 0, 0, 1}
        }}};

        return result;
    }

    inline auto rotation_3d_y(BaseType theta) {
        auto c_theta = std::cos(theta);
        auto s_theta = std::sin(theta);

        Transform3D result{{{
            { c_theta, 0, s_theta, 0 },
            { 0, 1, 0, 0 },
            { -s_theta, 0, c_theta, 0 },
            { 0, 0, 0, 1}
        }}};

        return result;
    }

    inline auto rotation_3d_z(BaseType theta) {
        auto c_theta = std::cos(theta);
        auto s_theta = std::sin(theta);

        Transform3D result{{{
            { c_theta, -s_theta, 0, 0 },
            { s_theta, c_theta, 0, 0 },
            { 0, 0, 1, 0 },
            { 0, 0, 0, 1}
        }}};

        return result;
    }

    inline auto rotation_3d(BaseType alpha, BaseType beta, BaseType gamma) {
        auto yaw_mat = rotation_3d_x(alpha);
        auto pitch_mat = rotation_3d_y(beta);
        auto roll_mat = rotation_3d_z(gamma);

        return roll_mat * pitch_mat * yaw_mat;
    }

    inline auto rotation_inverse_3d(BaseType alpha, BaseType beta, BaseType gamma) {
        auto yaw_mat = rotation_3d_x(-alpha);
        auto pitch_mat = rotation_3d_y(-beta);
        auto roll_mat = rotation_3d_z(-gamma);

        return yaw_mat * pitch_mat * roll_mat;
    }

    template<std::size_t N>
    constexpr inline auto scale(BaseType s) {
        Transform<N> result = transform_null<N>();
        for (std::size_t i = 0; i < N; ++i) {
            result[i][i] = s;
        }
        return result;
    }

    template<std::size_t N>
    constexpr inline auto scale(std::array<BaseType, N> s) {
        Transform<N> result = transform_null<N>();
        for (std::size_t i = 0; i < N; ++i) {
            result[i][i] = s[i];
        }
        return result;
    }

    template<std::size_t N>
    constexpr inline auto translation(std::array<BaseType, N> t) {
        Transform<N> result = transform_null<N>();
        for (std::size_t i = 0; i < N; ++i) {
            result[i][N] = t[i];
        }
        result[N][N] = 1;
        return result;
    }

    template<std::size_t N>
    constexpr inline auto translation_inverse(std::array<BaseType, N> t) {
        decltype(t) neg_t;
        for (std::size_t i = 0; i < N; ++i) {
            neg_t[i] = -t[i];
        }
        return translation(neg_t);
    }

    template<std::size_t N>
    constexpr inline auto projection() {
        return projection<N>(1);
    }

    template<std::size_t N>
    constexpr inline auto projection(BaseType d) {
        Projection<N> result{};
        for (std::size_t i = 0; i < N - 1; ++i) {
            result[i][i] = d;
        }
        result[N - 1][N - 1] = 1;
        return result;
    }
};

#endif
