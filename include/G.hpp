#ifndef INCLUDE_G_HPP
#define INCLUDE_G_HPP

#include "M.hpp"
#include "homogeneous.hpp"
#include <cassert>
#include <cmath>
#include <cstdint>
#include <functional>
#include <optional>
#include <numbers>
#include <numeric>
#include <utility>
#include <vector>

#include <iostream>

namespace G {
    using namespace M;

    class Object;

    ///////////
    // Types //
    ///////////

    using BaseType = Homogeneous::BaseType;
    using Coord3D = Homogeneous::Coord3D;
    using CanvasPosType = long;

    const auto sqrt2 = std::numbers::sqrt2;
    const auto inv_sqrt2 = 1.0 / sqrt2;

    const BaseType epsilon = 0.001;

    template<typename T>
    constexpr auto norm_sq(const T& a) {
        return std::transform_reduce(
            a.begin(), a.end(),
            typename T::value_type{},
            std::plus<>(),
            [](auto x){ return x * x; });
    }

    template<typename T>
    constexpr inline auto norm(const T& a) {
        return std::sqrt(norm_sq(a));
    }

    template<typename T>
    constexpr inline auto normalize(const T& a) {
        return a / norm(a);
    }

    template<typename T>
    constexpr auto dot(const T& a, const T& b) {
        return std::inner_product(a.begin(), a.end(), b.begin(), typename T::value_type{});
    }

    template<typename T>
    constexpr auto cos_between(const T& a, const T& b) {
        auto vect_dot = dot(a, b);
        return vect_dot / (norm(a) * norm(b));
    }

    template<typename T>
    constexpr auto cos_between_pos(const T& a, const T& b) {
        auto val = cos_between(a, b);
        if (val < 0) {
            return decltype(val){};
        } else {
            return val;
        }
    }

    ////////////
    // Colour //
    ////////////

    using ColourType = double;

    class Colour : public RowVector<ColourType, 3> {
    public:
        inline ColourType &r() { return (*this)[0]; };
        inline ColourType &g() { return (*this)[1]; };
        inline ColourType &b() { return (*this)[2]; };

        static const inline ColourType lower = 0.0;
        static const inline ColourType upper = 1.0;

        constexpr void clamp() {
            (*this)[0] = std::clamp((*this)[0], lower, upper);
            (*this)[1] = std::clamp((*this)[1], lower, upper);
            (*this)[2] = std::clamp((*this)[2], lower, upper);
        }
    };

    namespace Colours {
        // Greyscale
        inline constexpr Colour white     { 1.0, 1.0, 1.0 };
        inline constexpr Colour grey      { 0.5, 0.5, 0.5 };
        inline constexpr Colour black     { 0.0, 0.0, 0.0 };

        // Main colour wheel
        inline constexpr Colour red       { 1.0, 0.0, 0.0 };
        inline constexpr Colour rose      { 1.0, 0.0, 0.5 };
        inline constexpr Colour magenta   { 1.0, 0.0, 1.0 };
        inline constexpr Colour violet    { 0.5, 0.0, 1.0 };
        inline constexpr Colour blue      { 0.0, 0.0, 1.0 };
        inline constexpr Colour azure     { 0.0, 0.5, 1.0 };
        inline constexpr Colour cyan      { 0.0, 1.0, 1.0 };
        inline constexpr Colour aquamarine{ 0.0, 1.0, 0.5 };
        inline constexpr Colour lime      { 0.0, 1.0, 0.0 };
        inline constexpr Colour chartreuse{ 0.5, 1.0, 0.0 };
        inline constexpr Colour yellow    { 1.0, 1.0, 0.0 };
        inline constexpr Colour orange    { 1.0, 0.5, 0.0 };

        // Half-bright colours
        inline constexpr Colour maroon    { 0.5, 0.0, 0.0 };
        inline constexpr Colour purple    { 0.5, 0.0, 0.5 };
        inline constexpr Colour navy_blue { 0.0, 0.0, 0.5 };
        inline constexpr Colour teal      { 0.0, 0.5, 0.5 };
        inline constexpr Colour green     { 0.0, 0.5, 0.0 };
        inline constexpr Colour olive     { 0.5, 0.5, 0.0 };
    }

    ////////////
    // Camera //
    ////////////

    class Camera {
    public:
        constexpr inline auto translation() {
            return Homogeneous::translation<3>({ position[0], position[1], position[2] });
        }

        constexpr inline auto translation_inverse() {
            return Homogeneous::translation_inverse<3>({ position[0], position[1], position[2] });
        }

        inline auto rotation() {
            return Homogeneous::rotation_3d(yaw, pitch, roll);
        }

        inline auto rotation_inverse() {
            return Homogeneous::rotation_inverse_3d(yaw, pitch, roll);
        }

        Homogeneous::Transform3D non_inv_matrix() {
            return translation() * rotation();
        }

        Homogeneous::Transform3D inv_matrix() {
            return rotation_inverse() * translation_inverse();
        }

        Homogeneous::RawPoint<3> position;
        BaseType roll;
        BaseType pitch;
        BaseType yaw;
    };

    ////////////
    // Canvas //
    ////////////

    using CanvasPoint = ColVector<CanvasPosType, 2>;

    struct DepthedCanvasPoint {
        CanvasPoint point;
        BaseType z;
    };

    constexpr inline auto canvas_from_point(Homogeneous::RawPoint<2> point) {
        return CanvasPoint{
            static_cast<CanvasPosType>(point[0]),
            static_cast<CanvasPosType>(point[1])
        };
    }

    template <std::size_t M, std::size_t N>
    class Canvas;

    struct CanvasCell {
        Colour colour;
        BaseType depth;
    };

    template <std::size_t M, std::size_t N>
    class CanvasRow {
    protected:
        CanvasRow(MatrixRow<CanvasCell, N> &matrix_row) : matrix_row(matrix_row) {}

        friend Canvas<M, N>;

    public:
        constexpr auto &at(CanvasPosType pos) {
            CanvasPosType offset_pos = pos + N / 2;
            if (offset_pos >= 0 && static_cast<std::size_t>(offset_pos) < N) {
                return matrix_row.at(offset_pos);
            } else {
                return dummy_cell;
            }
        }

        constexpr auto &operator[](CanvasPosType pos) {
            CanvasPosType offset_pos = pos + N / 2;
            if (offset_pos >= 0 && static_cast<std::size_t>(offset_pos) < N) {
                return matrix_row.operator[](offset_pos);
            } else {
                return dummy_cell;
            }
        }

        constexpr auto &raw_at(std::size_t pos) {
            return matrix_row.at(pos);
        }

        constexpr auto &raw_idx(std::size_t pos) {
            return matrix_row.operator[](pos);
        }

    private:
        MatrixRow<CanvasCell, N> &matrix_row;
        CanvasCell dummy_cell;
    };

    template <std::size_t M, std::size_t N>
    class Canvas : public Matrix<CanvasCell, M, N> {
    public:
        constexpr inline auto at(CanvasPosType pos) {
            CanvasPosType offset_pos = pos + M / 2;
            if (offset_pos >= 0 && static_cast<std::size_t>(offset_pos) < M) {
                return CanvasRow<M, N>(raw_at(offset_pos));
            } else {
                return CanvasRow<M, N>(dummy_row);
            }
        }

        constexpr inline auto operator[](CanvasPosType pos) {
            CanvasPosType offset_pos = pos + M / 2;
            if (offset_pos >= 0 && static_cast<std::size_t>(offset_pos) < M) {
                return CanvasRow<M, N>(raw_idx(offset_pos));
            } else {
                return CanvasRow<M, N>(dummy_row);
            }
        }

        constexpr inline auto &raw_at(std::size_t pos) {
            return Matrix<CanvasCell, M, N>::at(pos);
        }

        constexpr inline auto &raw_idx(std::size_t pos) {
            return Matrix<CanvasCell, M, N>::operator[](pos);
        }

        constexpr inline bool point_is_valid(CanvasPoint point) {
            CanvasPosType raw_x = point[0] + N / 2;
            CanvasPosType raw_y = point[1] + M / 2;
            return (
                raw_y >= 0 && static_cast<std::size_t>(raw_y) < M &&
                raw_x >= 0 && static_cast<std::size_t>(raw_x) < N
            );
        }

        constexpr inline void put_pixel_always(CanvasPoint point, BaseType inv_z, Colour colour) {
            auto x = point[0];
            auto y = point[1];

            (*this)[y][x].colour = colour;
            (*this)[y][x].depth = inv_z;
        }

        constexpr inline void put_pixel(CanvasPoint point, BaseType inv_z, Colour colour) {
            auto x = point[0];
            auto y = point[1];

            if (inv_z > (*this)[y][x].depth) {
                (*this)[y][x].colour = colour;
                (*this)[y][x].depth = inv_z;
            }
        }

        constexpr void clamp() {
            for (auto &row : *this) {
                for (auto &cell : row) {
                    cell.colour.clamp();
                }
            }
        }
        
        constexpr void write_out(unsigned char out[M][N][3]) {
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    auto &c = raw_at(i).at(j);
                    out[i][j][0] = c.b() * 255;
                    out[i][j][1] = c.g() * 255;
                    out[i][j][2] = c.r() * 255;
                }
            }
        }
        
        constexpr void write_out(std::array<uint8_t, M * N * 3> &out) {
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    auto &c = raw_at(i).at(j);
                    std::size_t pixel_index = (i * N) + j;
                    out[pixel_index * 3 + 0] = c.colour.b() * 255;
                    out[pixel_index * 3 + 1] = c.colour.g() * 255;
                    out[pixel_index * 3 + 2] = c.colour.r() * 255;
                }
            }
        }

    private:
        MatrixRow<CanvasCell, N> dummy_row; // Used for out-of-bounds accesses
    };

    //////////////
    // Viewport //
    //////////////

    class Viewport {
    public:
        constexpr Viewport(Homogeneous::RawPoint<2> viewport_size, BaseType plane_distance, CanvasPoint canvas_size) :
            viewport_size(viewport_size), plane_distance(plane_distance), canvas_size(canvas_size) {}

        constexpr Homogeneous::RawPoint<3> pos_from_canvas(CanvasPoint canvas_point) {
            return {
                canvas_point[0] * (viewport_size[0] / canvas_size[0]),
                canvas_point[1] * (viewport_size[1] / canvas_size[1]),
                plane_distance
            };
        }

        constexpr CanvasPoint pos_to_canvas(Homogeneous::RawPoint<2> viewport_point) {
            return {
                static_cast<CanvasPosType>(viewport_point[0] * (canvas_size[0] / viewport_size[0])),
                static_cast<CanvasPosType>(viewport_point[1] * (canvas_size[1] / viewport_size[1]))
            };
        }

        constexpr CanvasPoint project_vertex(Coord3D point) {
            return pos_to_canvas({
                point[0] * (plane_distance / point[2]),
                point[1] * (plane_distance / point[2])
            });
        }

        constexpr inline auto m_viewport_to_canvas() {
            return Homogeneous::scale<2>({
                canvas_size[0] / viewport_size[0],
                canvas_size[1] / viewport_size[1]
            });
        }

        constexpr inline auto m_3d_to_canvas() {
            Homogeneous::Projection3D result =
                m_viewport_to_canvas() * Homogeneous::projection<3>(plane_distance);
            return result;
        }

        Homogeneous::RawPoint<2> viewport_size;
        BaseType plane_distance;
        CanvasPoint canvas_size;
    };

    ////////////
    // Raster //
    ////////////

    namespace Raster {
        template <typename T>
        std::vector<T> interpolate(CanvasPosType i0, T d0, CanvasPosType i1, T d1) {
            if (i0 == i1) {
                return { d0 };
            }

            auto a = (d1 - d0) / (i1 - i0);
            auto d = d0;

            std::vector<T> values{};
            for (CanvasPosType i = i0; i <= i1; ++i) {
                values.emplace_back(d);
                d += a;
            }
            return values;
        }

        BaseType signed_distance(Homogeneous::Plane3D &plane, Coord3D vertex) {
            return (plane * vertex)[0];
        }

        Coord3D intersect(Coord3D &a, Coord3D &b, Homogeneous::Plane3D &plane) {
            auto t = (-plane[3] - (plane * (a - Coord3D{ 0, 0, 0, 1 }))[0]) / (plane * (b - a))[0];
            return a + t * (b - a);
        }

        template <std::size_t M, std::size_t N>
        void draw_line(
            Canvas<M, N> &canvas,
            DepthedCanvasPoint p0,
            DepthedCanvasPoint p1,
            Colour colour
        ) {
            if (std::abs(p1.point[0] - p0.point[0]) > std::abs(p1.point[1] - p0.point[1])) {
                if (p0.point[0] > p1.point[0]) {
                    std::swap(p0, p1);
                }

                BaseType x0 = p0.point[0];
                BaseType y0 = p0.point[1];
                BaseType z0 = p0.z;

                BaseType x1 = p1.point[0];
                BaseType y1 = p1.point[1];
                BaseType z1 = p1.z;

                BaseType inv_z0 = 1 / z0;
                BaseType inv_z1 = 1 / z1;

                auto ys = interpolate(x0, y0, x1, y1);
                auto inv_zs = interpolate(x0, inv_z0, x1, inv_z1);

                for (CanvasPosType x = x0; x < x1; ++x) {
                    CanvasPosType y = ys[x - x0];
                    auto inv_z = inv_zs[x - x0];

                    canvas.put_pixel({ x, y }, inv_z, colour);
                }
            } else {
                if (p0.point[1] > p1.point[1]) {
                    std::swap(p0, p1);
                }

                BaseType x0 = p0.point[0];
                BaseType y0 = p0.point[1];
                BaseType z0 = p0.z;

                BaseType x1 = p1.point[0];
                BaseType y1 = p1.point[1];
                BaseType z1 = p1.z;

                BaseType inv_z0 = 1 / z0;
                BaseType inv_z1 = 1 / z1;

                auto xs = interpolate(y0, x0, y1, x1);
                auto inv_zs = interpolate(y0, inv_z0, y1, inv_z1);

                for (CanvasPosType y = y0; y < y1; ++y) {
                    CanvasPosType x = xs[y - y0];
                    auto inv_z = inv_zs[y - y0];

                    canvas.put_pixel({ x, y }, inv_z, colour);
                }
            }
        }

        template <std::size_t M, std::size_t N>
        void draw_wireframe_triangle(Canvas<M, N> &canvas,
            DepthedCanvasPoint p0,
            DepthedCanvasPoint p1,
            DepthedCanvasPoint p2,
            Colour colour
        ) {
            draw_line(canvas, p0, p1, colour);
            draw_line(canvas, p1, p2, colour);
            draw_line(canvas, p2, p0, colour);
        }

        template <std::size_t M, std::size_t N>
        void draw_filled_triangle(Canvas<M, N> &canvas,
            DepthedCanvasPoint p0,
            DepthedCanvasPoint p1,
            DepthedCanvasPoint p2,
            Colour colour
        ) {
            if (p1.point[1] < p0.point[1]) {
                std::swap(p1, p0);
            }
            if (p2.point[1] < p0.point[1]) {
                std::swap(p2, p0);
            }
            if (p2.point[1] < p1.point[1]) {
                std::swap(p2, p1);
            }

            BaseType x0 = p0.point[0];
            BaseType y0 = p0.point[1];
            BaseType z0 = p0.z;

            BaseType x1 = p1.point[0];
            BaseType y1 = p1.point[1];
            BaseType z1 = p1.z;

            BaseType x2 = p2.point[0];
            BaseType y2 = p2.point[1];
            BaseType z2 = p2.z;

            auto inv_z0 = 1 / z0;
            auto inv_z1 = 1 / z1;
            auto inv_z2 = 1 / z2;

            auto x01 = interpolate(y0, x0, y1, x1);
            auto inv_z01 = interpolate(y0, inv_z0, y1, inv_z1);

            auto x12 = interpolate(y1, x1, y2, x2);
            auto inv_z12 = interpolate(y1, inv_z1, y2, inv_z2);

            auto x02 = interpolate(y0, x0, y2, x2);
            auto inv_z02 = interpolate(y0, inv_z0, y2, inv_z2);

            x01.pop_back();
            auto x012 = x01;
            x012.insert(x012.end(), x12.begin(), x12.end());

            inv_z01.pop_back();
            auto inv_z012 = inv_z01;
            inv_z012.insert(inv_z012.end(), inv_z12.begin(), inv_z12.end());

            std::size_t m = x012.size() / 2;
            std::vector<BaseType> *x_left;
            std::vector<BaseType> *inv_z_left;
            std::vector<BaseType> *x_right;
            std::vector<BaseType> *inv_z_right;
            if (x02[m] < x012[m]) {
                x_left = &x02;
                inv_z_left = &inv_z02;

                x_right = &x012;
                inv_z_right = &inv_z012;
            } else {
                x_left = &x012;
                inv_z_left = &inv_z012;
    
                x_right = &x02;
                inv_z_right = &inv_z02;
            }

            for (CanvasPosType y = y0; y < y2; ++y) {
                CanvasPosType x_l = (*x_left)[y - y0];
                CanvasPosType x_r = (*x_right)[y - y0];

                auto inv_z_l = (*inv_z_left)[y - y0];
                auto inv_z_r = (*inv_z_right)[y - y0];

                auto inv_z_segment = interpolate(x_l, inv_z_l, x_r, inv_z_r);

                for (CanvasPosType x = x_l; x <= x_r; ++x) {
                    auto inv_z = inv_z_segment[x - x_l];
                    canvas.put_pixel({ x, y }, inv_z, colour);
                }
            }
        }

        template <std::size_t M, std::size_t N>
        constexpr void draw_shaded_triangle(
            Canvas<M, N> &canvas,
            DepthedCanvasPoint p0, BaseType h0,
            DepthedCanvasPoint p1, BaseType h1,
            DepthedCanvasPoint p2, BaseType h2,
            Colour colour
        ) {
            if (p1.point[1] < p0.point[1]) {
                std::swap(p1, p0);
            }
            if (p2.point[1] < p0.point[1]) {
                std::swap(p2, p0);
            }
            if (p2.point[1] < p1.point[1]) {
                std::swap(p2, p1);
            }

            BaseType x0 = p0.point[0];
            BaseType y0 = p0.point[1];
            BaseType z0 = p0.z;

            BaseType x1 = p1.point[0];
            BaseType y1 = p1.point[1];
            BaseType z1 = p1.z;

            BaseType x2 = p2.point[0];
            BaseType y2 = p2.point[1];
            BaseType z2 = p2.z;

            auto inv_z0 = 1 / z0;
            auto inv_z1 = 1 / z1;
            auto inv_z2 = 1 / z2;

            auto x01 = interpolate(y0, x0, y1, x1);
            auto inv_z01 = interpolate(y0, inv_z0, y1, inv_z1);
            auto h01 = interpolate(y0, h0, y1, h1);

            auto x12 = interpolate(y1, x1, y2, x2);
            auto inv_z12 = interpolate(y1, inv_z1, y2, inv_z2);
            auto h12 = interpolate(y1, h1, y2, h2);

            auto x02 = interpolate(y0, x0, y2, x2);
            auto inv_z02 = interpolate(y0, inv_z0, y2, inv_z2);
            auto h02 = interpolate(y0, h0, y2, h2);

            x01.pop_back();
            auto x012 = x01;
            x012.insert(x012.end(), x12.begin(), x12.end());

            inv_z01.pop_back();
            auto inv_z012 = inv_z01;
            inv_z012.insert(inv_z012.end(), inv_z12.begin(), inv_z12.end());

            h01.pop_back();
            auto h012 = h01;
            h012.insert(h012.end(), h12.begin(), h12.end());

            std::size_t m = x012.size() / 2;
            std::vector<BaseType> *x_left;
            std::vector<BaseType> *inv_z_left;
            std::vector<BaseType> *h_left;
            std::vector<BaseType> *x_right;
            std::vector<BaseType> *inv_z_right;
            std::vector<BaseType> *h_right;
            if (x02[m] < x012[m]) {
                x_left = &x02;
                inv_z_left = &inv_z02;
                h_left = &h02;

                x_right = &x012;
                inv_z_right = &inv_z012;
                h_right = &h012;
            } else {
                x_left = &x012;
                inv_z_left = &inv_z012;
                h_left = &h012;

                x_right = &x02;
                inv_z_right = &inv_z02;
                h_right = &h02;
            }

            for (CanvasPosType y = y0; y < y2; ++y) {
                CanvasPosType x_l = (*x_left)[y - y0];
                CanvasPosType x_r = (*x_right)[y - y0];

                auto inv_z_l = (*inv_z_left)[y - y0];
                auto inv_z_r = (*inv_z_right)[y - y0];

                BaseType h_l = (*h_left)[y - y0];
                BaseType h_r = (*h_right)[y - y0];

                auto inv_z_segment = interpolate(x_l, inv_z_l, x_r, inv_z_r);
                auto h_segment = interpolate(x_l, h_l, x_r, h_r);

                for (CanvasPosType x = x_l; x <= x_r; ++x) {
                    auto inv_z = inv_z_segment[x - x_l];
                    auto shaded_colour = Colour{colour * h_segment[x - x_l]};
                    canvas.put_pixel({ x, y }, inv_z, shaded_colour);
                }
            }
        }

        using VertexIndex = std::size_t;

        class VertexIndexTriangle {
        public:
            std::array<VertexIndex, 3> vertex_indices;
            std::array<Coord3D, 3> vertex_normals;
        };

        class ColouredVertexIndexTriangle : public VertexIndexTriangle {
        public:
            Colour colour;
        };

        class VertexTriangle {
        public:
            constexpr VertexTriangle(
                const VertexIndexTriangle &vert_idx_triangle,
                const std::vector<Coord3D> &raw_vertices,
                Colour colour
            ) : colour(colour)  {
                auto &vert_idx = vert_idx_triangle.vertex_indices;
                vertices[0] = raw_vertices.at(vert_idx[0]);
                vertices[1] = raw_vertices.at(vert_idx[1]);
                vertices[2] = raw_vertices.at(vert_idx[2]);
                vertex_normals = vert_idx_triangle.vertex_normals;
            }

            constexpr VertexTriangle(
                const VertexIndexTriangle &vert_idx_triangle,
                const std::vector<Homogeneous::RawPoint<3>> &raw_vertices,
                Colour colour
            ) : colour(colour) {
                auto &vert_idx = vert_idx_triangle.vertex_indices;
                vertices[0] = Homogeneous::point_from_raw<3>(raw_vertices.at(vert_idx[0]));
                vertices[1] = Homogeneous::point_from_raw<3>(raw_vertices.at(vert_idx[1]));
                vertices[2] = Homogeneous::point_from_raw<3>(raw_vertices.at(vert_idx[2]));
                vertex_normals = vert_idx_triangle.vertex_normals;
            }

            constexpr VertexTriangle(
                const ColouredVertexIndexTriangle &vert_idx_triangle,
                const std::vector<Coord3D> &raw_vertices
            ) : VertexTriangle(static_cast<const VertexIndexTriangle &>(vert_idx_triangle), raw_vertices, vert_idx_triangle.colour) {}

            constexpr VertexTriangle(
                const ColouredVertexIndexTriangle &vert_idx_triangle,
                const std::vector<Homogeneous::RawPoint<3>> &raw_vertices
            ) : VertexTriangle(static_cast<const VertexIndexTriangle &>(vert_idx_triangle), raw_vertices, vert_idx_triangle.colour) {}

            constexpr VertexTriangle(std::array<Coord3D, 3> vertices, std::array<Coord3D, 3> vertex_normals, Colour colour) :
                vertices(vertices), vertex_normals(vertex_normals), colour(colour) {}

            constexpr bool should_cull() {
                // TODO: Implement
                return false;
            }

            std::vector<VertexTriangle> clip_against_plane(Homogeneous::Plane3D &plane) {
                auto d0 = signed_distance(plane, vertices[0]);
                auto d1 = signed_distance(plane, vertices[1]);
                auto d2 = signed_distance(plane, vertices[2]);

                unsigned count_positive = 0;
                if (d0 >= 0) {
                    count_positive += 1;
                }
                if (d1 >= 0) {
                    count_positive += 1;
                }
                if (d2 >= 0) {
                    count_positive += 1;
                }

                if (count_positive == 3) {
                    // All positive
                    return { *this };
                } else if (count_positive == 0) {
                    // All negative
                    return {};
                } else if (count_positive == 1) {
                    Coord3D a;
                    Coord3D b;
                    Coord3D c;

                    if (d0 >= 0) {
                        a = vertices[0];
                        b = vertices[1];
                        c = vertices[2];
                    } else if (d1 >= 0) {
                        a = vertices[1];
                        b = vertices[2];
                        c = vertices[0];
                    } else {
                        a = vertices[2];
                        b = vertices[0];
                        c = vertices[1];
                    }

                    Coord3D b_prime = intersect(a, b, plane);
                    Coord3D c_prime = intersect(a, c, plane);

                    return std::vector<VertexTriangle>{
                        VertexTriangle(
                            { a, b_prime, c_prime },
                            {}, // TODO: Find new normals
                            colour
                        )
                    };
                } else {
                    Coord3D a;
                    Coord3D b;
                    Coord3D c;

                    if (d0 < 0) {
                        a = vertices[1];
                        b = vertices[2];
                        c = vertices[0];
                    } else if (d1 < 0) {
                        a = vertices[2];
                        b = vertices[0];
                        c = vertices[1];
                    } else {
                        a = vertices[0];
                        b = vertices[1];
                        c = vertices[2];
                    }

                    Coord3D a_prime = intersect(a, c, plane);
                    Coord3D b_prime = intersect(b, c, plane);

                    return {
                        VertexTriangle{
                            { a, b, a_prime },
                            {}, // TODO: Find new normals
                            colour
                        },
                        VertexTriangle{
                            { a_prime, b, b_prime },
                            {}, // TODO: Find new normals
                            colour
                        }
                    };
                }
            }

            template <std::size_t M, std::size_t N>
            inline void render(Canvas<M, N> &canvas, Homogeneous::Projection3D m_projection) {
                auto projected_0 = m_projection * vertices[0];
                auto projected_1 = m_projection * vertices[1];
                auto projected_2 = m_projection * vertices[2];

                auto p0 = Homogeneous::point_to_raw<2>(projected_0);
                auto p1 = Homogeneous::point_to_raw<2>(projected_1);
                auto p2 = Homogeneous::point_to_raw<2>(projected_2);

                // draw_wireframe_triangle(
                draw_filled_triangle(
                    canvas,
                    { static_cast<CanvasPosType>(p0[0]), static_cast<CanvasPosType>(p0[1]), projected_0[2] },
                    { static_cast<CanvasPosType>(p1[0]), static_cast<CanvasPosType>(p1[1]), projected_1[2] },
                    { static_cast<CanvasPosType>(p2[0]), static_cast<CanvasPosType>(p2[1]), projected_2[2] },
                    colour);
            }

            std::array<Coord3D, 3> vertices;
            std::array<Coord3D, 3> vertex_normals;
            Colour colour;
        };

        class IndexModel {
        public:
            std::vector<Homogeneous::RawPoint<3>> vertices;
            std::vector<ColouredVertexIndexTriangle> triangles;
        };

        class Model {
        public:
            Model(IndexModel &idx_model) {
                for (auto &i_t : idx_model.triangles) {
                    triangles.emplace_back(i_t, idx_model.vertices);
                }
            }

            void transform(Homogeneous::Transform3D &m_scene) {
                for (auto &t : triangles) {
                    t.vertices[0] = m_scene * t.vertices[0];
                    t.vertices[1] = m_scene * t.vertices[1];
                    t.vertices[2] = m_scene * t.vertices[2];
                }
            }

            void cull_back_faces() {
                std::vector<VertexTriangle> culled_triangles;
                for (auto &t : triangles) {
                    if (!t.should_cull()) {
                        culled_triangles.emplace_back(t);
                    }
                }
                triangles = culled_triangles;
            }

            void clip_against_plane(Homogeneous::Plane3D &plane) {
                std::vector<VertexTriangle> clipped_triangles;
                for (auto &t : triangles) {
                    auto new_clipped_triangles = t.clip_against_plane(plane);
                    clipped_triangles.insert(clipped_triangles.end(), new_clipped_triangles.begin(), new_clipped_triangles.end());
                }
                triangles = clipped_triangles;
            }

            void clip(std::vector<Homogeneous::Plane3D> &planes) {
                for (auto &p : planes) {
                    clip_against_plane(p);
                }
            }

            template <std::size_t M, std::size_t N>
            void render(Canvas<M, N> &canvas, Homogeneous::Projection3D m_projection) {
                for (auto &t : triangles) {
                    t.render(canvas, m_projection);
                }
            }

            std::vector<VertexTriangle> triangles = {};
        };

        class Instance {
        public:
            inline bool should_cull(std::vector<Homogeneous::Plane3D> &planes) {
                // TODO: Implement
                return false;
            }

            inline void transform(Homogeneous::Transform3D &m_scene) {
                auto m = m_scene * m_model;
                model.transform(m);
            }

            inline void cull_back_faces() {
                model.cull_back_faces();
            }

            inline void clip(std::vector<Homogeneous::Plane3D> &planes) {
                model.clip(planes);
            }

            template <std::size_t M, std::size_t N>
            inline void render(Canvas<M, N> &canvas, Homogeneous::Projection3D m_projection) {
                model.render(canvas, m_projection);
            }

            Model model;
            Homogeneous::Transform3D m_model;
        };

        class Scene {
        public:
            template <std::size_t M, std::size_t N>
            void render(Canvas<M, N> &canvas, Homogeneous::Projection3D m_projection, Homogeneous::Transform3D m_camera) {
                std::vector<Homogeneous::Plane3D> planes = {
                    { 0, 0, 1, -1 },                    // near
                    { inv_sqrt2, 0, inv_sqrt2, 0 },     // left
                    { -inv_sqrt2, 0, inv_sqrt2, 0 },    // right
                    { 0, inv_sqrt2, inv_sqrt2, 0 },     // bottom
                    { 0, -inv_sqrt2, inv_sqrt2, 0 },    // top
                };

                std::vector<Instance> render_instances = instances;

                for (auto &inst : render_instances) {
                    inst.transform(m_camera);
                    if (inst.should_cull(planes)) {
                        continue;
                    }
                    inst.cull_back_faces();
                    inst.clip(planes);
                    inst.render(canvas, m_projection);
                }
            }

            std::vector<Instance> instances;
        };
    };

    /////////////
    // Raycast //
    /////////////

    namespace Raycast {
        /////////////
        // Objects //
        /////////////

        class Object {
        public:
            static std::optional<std::pair<BaseType, Object *>> closest_intersection(
                std::vector<Object *> &objects,
                Coord3D origin, Coord3D ray_dir,
                BaseType t_min, BaseType t_max
            ) {
                assert(Homogeneous::is_point<3>(origin));
                assert(Homogeneous::is_vector<3>(ray_dir));

                BaseType t_closest = INFINITY;
                Object *obj_closest = nullptr;

                for (auto &obj : objects) {
                    auto t = obj->intersect_ray(origin, ray_dir, t_min, t_max);
                    if (!std::isnan(t) && t < t_closest) {
                        t_closest = t;
                        obj_closest = obj;
                    }
                }

                if (std::isfinite(t_closest)) {
                    return std::pair<BaseType, Object *>{t_closest, obj_closest};
                } else {
                    return std::nullopt;
                }
            }

            virtual constexpr BaseType intersect_ray(Coord3D origin, Coord3D ray_dir, BaseType t_min, BaseType t_max) = 0;
            virtual constexpr std::optional<Colour> colour_at_ray(Coord3D origin, Coord3D ray_dir, BaseType t) = 0;
            virtual constexpr std::optional<Coord3D> normal_at_ray(Coord3D origin, Coord3D ray_dir, BaseType t) = 0;
            virtual constexpr std::optional<BaseType> specular_at_ray(Coord3D origin, Coord3D ray_dir, BaseType t) = 0;
            virtual constexpr std::optional<BaseType> reflective_at_ray(Coord3D origin, Coord3D ray_dir, BaseType t) = 0;
        };

        class Sphere : public Object {
        public:
            constexpr Sphere(Coord3D centre, BaseType radius) : centre(centre), radius(radius) {
                assert(Homogeneous::is_point<3>(centre));
                assert(radius >= 0);
            }

            constexpr BaseType intersect_ray(Coord3D origin, Coord3D ray_dir, BaseType t_min, BaseType t_max) override {
                assert(Homogeneous::is_point<3>(origin));
                assert(Homogeneous::is_vector<3>(ray_dir));

                auto co = origin - centre;

                auto a = dot(ray_dir, ray_dir);
                auto b = 2 * dot(co, ray_dir);
                auto c = dot(co, co) - (radius * radius);

                auto discriminant = (b * b) - (4 * a * c);
                if (discriminant < 0) {
                    return NAN;
                }

                auto t1 = (-b + std::sqrt(discriminant)) / (2 * a);
                auto t2 = (-b - std::sqrt(discriminant)) / (2 * a);

                if (t1 < t_min || t_max < t1) {
                    t1 = NAN;
                }
                if (t2 < t_min || t_max < t2) {
                    t2 = NAN;
                }

                auto t = std::isnan(t1) ? t2 : std::isnan(t2) ? t1 : std::min(t1, t2);
                return t;
            }

            constexpr std::optional<Coord3D> normal_at_ray(Coord3D origin, Coord3D ray_dir, BaseType t) override {
                auto intr_point = origin + t * ray_dir;
                auto normal = intr_point - centre;
                return normalize(normal);
            }

            Coord3D centre;
            BaseType radius;
        };

        class MonochromeSphere : public Sphere {
        public:
            constexpr MonochromeSphere(Coord3D centre, BaseType radius, Colour colour, BaseType specular, BaseType reflective) :
                Sphere(centre, radius), colour(colour), specular(specular), reflective(reflective) {}

            constexpr std::optional<Colour> colour_at_ray(Coord3D, Coord3D, BaseType) override { return colour; }
            constexpr std::optional<BaseType> specular_at_ray(Coord3D, Coord3D, BaseType) override { return specular; }
            constexpr std::optional<BaseType> reflective_at_ray(Coord3D, Coord3D, BaseType) override { return reflective; }

            Colour colour;
            BaseType specular;
            BaseType reflective;
        };

        //////////////
        // Lighting //
        //////////////

        using LightValue = double;

        class LightSource {
        public:
            constexpr LightSource(LightValue intensity) : intensity(intensity) {}

            virtual constexpr LightValue get_lighting(
                std::vector<Object *> &objects,
                Coord3D pos, Coord3D normal_dir, Homogeneous::Coord <3> view_dir, BaseType specular
            ) = 0;

            static LightValue compute_lighting(
                std::vector<Object *> &objects, std::vector<LightSource *> &light_sources,
                Coord3D pos, Coord3D normal_dir, Coord3D view_dir, BaseType specular
            ) {
                LightValue lighting = 0;

                for (auto src : light_sources) {
                    lighting += src->get_lighting(objects, pos, normal_dir, view_dir, specular);
                }

                return lighting;
            }

            LightValue intensity;
        };

        class AmbientLightSource : public LightSource {
        public:
            constexpr AmbientLightSource(LightValue intensity) : LightSource(intensity) {}

            constexpr LightValue get_lighting(
                std::vector<Object *> &,
                Coord3D, Coord3D, Coord3D, BaseType
            ) override {
                return intensity;
            }
        };

        class VectLightSource : public LightSource {
        public:
            constexpr VectLightSource(LightValue intensity) : LightSource(intensity) {}

            LightValue get_lighting(
                std::vector<Object *> &objects,
                Coord3D pos, Coord3D normal_dir, Coord3D view_dir, BaseType specular
            ) override {
                assert(Homogeneous::is_point<3>(pos));
                auto light_dir = get_light_direction(pos);

                auto shadow = Object::closest_intersection(objects, pos, light_dir, epsilon, get_t_max());
                if (shadow.has_value()) {
                    return 0;
                }

                // Diffuse
                auto cos_n_l = cos_between_pos(normal_dir, light_dir);
                auto diff = cos_n_l;

                // Specular
                auto refl_dir = 2 * normal_dir * dot(normal_dir, light_dir) - light_dir;
                auto cos_r_v = cos_between_pos(refl_dir, view_dir);
                auto spec = std::isnan(specular) ? 0 : std::pow(cos_r_v, specular);

                return intensity * (diff + spec);
            }

            virtual constexpr Coord3D get_light_direction(Coord3D pos) = 0;
            virtual constexpr BaseType get_t_max() = 0;
        };

        class DirectionalLightSource : public VectLightSource {
        public:
            constexpr DirectionalLightSource(LightValue intensity, Coord3D direction) :
                VectLightSource(intensity), direction(direction)
            {
                assert(Homogeneous::is_vector<3>(direction));
            }

            constexpr Coord3D get_light_direction(Coord3D) override {
                return direction;
            }
            
            constexpr BaseType get_t_max() override {
                return INFINITY;
            }

            Coord3D direction;
        };

        class PointLightSource : public VectLightSource {
        public:
            constexpr PointLightSource(LightValue intensity, Coord3D position) :
                VectLightSource(intensity), position(position)
            {
                assert(Homogeneous::is_point<3>(position));
            }

            constexpr Coord3D get_light_direction(Coord3D pos) override {
                return position - pos;
            }
            
            constexpr BaseType get_t_max() override {
                return 1;
            }

            Coord3D position;
        };

        constexpr static Coord3D reflect_ray(Coord3D ray, Coord3D normal) {
            return 2 * normal * dot(normal, ray) - ray;
        }

        static Colour trace_ray(
            std::vector<Object *> &objects, std::vector<LightSource *> &lights,
            Coord3D origin, Coord3D ray_dir,
            BaseType t_min, BaseType t_max,
            std::size_t recursion_depth
        ) {
            assert(Homogeneous::is_point<3>(origin));
            assert(Homogeneous::is_vector<3>(ray_dir));

            auto closest = Object::closest_intersection(objects, origin, ray_dir, t_min, t_max);

            if (!closest.has_value()) {
                return {};
            }

            auto t = closest.value().first;
            auto obj = closest.value().second;

            auto point = origin + t * ray_dir;
            auto normal = obj->normal_at_ray(origin, ray_dir, t).value();
            auto specular = obj->specular_at_ray(origin, ray_dir, t).value();
            auto light = LightSource::compute_lighting(objects, lights, point, normal, -ray_dir, specular);
            auto raw_colour = obj->colour_at_ray(origin, ray_dir, t).value();
            auto local_colour = Colour{light * raw_colour};

            auto r = obj->reflective_at_ray(origin, ray_dir, t).value();
            if (recursion_depth <= 0 || r <= 0) {
                return local_colour;
            }

            auto refl_ray_dir = reflect_ray(-ray_dir, normal);
            auto reflected_colour = trace_ray(
                objects, lights,
                point, refl_ray_dir,
                epsilon, INFINITY,
                recursion_depth - 1
            );

            return Colour{ (1 - r) * local_colour + r * reflected_colour };
        }
    }
}

#endif
