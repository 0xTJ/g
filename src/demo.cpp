#include "G.hpp"
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace G;

const int BYTES_PER_PIXEL = 3; /// red, green, & blue
const int FILE_HEADER_SIZE = 14;
const int INFO_HEADER_SIZE = 40;

void generateBitmapImage(unsigned char* image, int height, int width, const char* imageFileName);
unsigned char* createBitmapFileHeader(int height, int stride);
unsigned char* createBitmapInfoHeader(int height, int width);

const int c_w = 1000;
const int c_h = 1000;

Canvas<c_h, c_w> raycast_canvas;
std::array<unsigned char, c_h * c_w * 3> raycast_image;

Canvas<c_h, c_w> raster_canvas;
std::array<unsigned char, c_h * c_w * 3> raster_image;

int main() {
    std::cout << "Running raycast\n";

    Raycast::MonochromeSphere my_sphere1({0, -1, 3, 1}, 1, {1, 0, 0}, 500, 0.2);
    Raycast::MonochromeSphere my_sphere2({2, 0, 6, 1}, 1, {0, 0, 1}, 500, 0.3);
    Raycast::MonochromeSphere my_sphere3({-2, 0, 6, 1}, 1, {0, 1, 0}, 10, 0.4);
    Raycast::MonochromeSphere my_sphere4({0, -5001, 0, 1}, 5000, {1, 1, 0}, 1000, 0.5);
    std::vector<Raycast::Object *> my_spheres = { &my_sphere1, &my_sphere2, &my_sphere3, &my_sphere4 };

    Raycast::AmbientLightSource my_light0(0.2);
    Raycast::PointLightSource my_light1(0.6, {2, 1, 0, 1});
    Raycast::DirectionalLightSource my_light2(0.2, {1, 4, 4, 0});
    std::vector<Raycast::LightSource *> my_lights = { &my_light0, &my_light1, &my_light2 };

    Viewport my_viewport({1, 1}, 1, {c_w, c_h});

    Camera my_camera{
        .position = { 0, 0, 0 },
        .roll = 0,
        .pitch = 0,
        .yaw = 0
    };

    for (int c_y = (-c_h) / 2; c_y < (c_h + 1) / 2; ++c_y) {
        for (int c_x = (-c_w) / 2; c_x < (c_w + 1) / 2; ++c_x) {
            auto origin = my_camera.translation() * Homogeneous::point_null<3>();
            auto ray = my_camera.rotation() * Homogeneous::vect_from_raw(my_viewport.pos_from_canvas({ c_x, c_y }));

            auto pixel = trace_ray(
                my_spheres, my_lights,
                origin, ray,
                my_viewport.plane_distance, INFINITY, 3);
            raycast_canvas.put_pixel_always({ c_x, c_y }, 0, pixel);
        }
    }

    raycast_canvas.clamp();
    raycast_canvas.write_out(raycast_image);
    std::cout << "Writing out raycast\n";
    generateBitmapImage(raycast_image.data(), c_w, c_h, "out/raycast.bmp");
    
    std::cout << "Running raster\n";

    Raster::IndexModel cube_index_model{
        .vertices = {
            {  1,  1,  1 },
            { -1,  1,  1 },
            { -1, -1,  1 },
            {  1, -1,  1 },
            {  1,  1, -1 },
            { -1,  1, -1 },
            { -1, -1, -1 },
            {  1, -1, -1 },
        },
        .triangles = {
            Raster::ColouredVertexIndexTriangle{ { { 0, 1, 2 }, { 0, 0, 1 } }, Colours::red },
            Raster::ColouredVertexIndexTriangle{ { { 0, 2, 3 }, { 0, 0, 1 } }, Colours::rose },
            Raster::ColouredVertexIndexTriangle{ { { 4, 0, 3 }, { 1, 0, 0 } }, Colours::magenta },
            Raster::ColouredVertexIndexTriangle{ { { 4, 3, 7 }, { 1, 0, 0 } }, Colours::violet },
            Raster::ColouredVertexIndexTriangle{ { { 5, 4, 7 }, { 0, 0, -1 } }, Colours::blue },
            Raster::ColouredVertexIndexTriangle{ { { 5, 7, 6 }, { 0, 0, -1 } }, Colours::azure },
            Raster::ColouredVertexIndexTriangle{ { { 1, 5, 6 }, { -1, 0, 0 } }, Colours::cyan },
            Raster::ColouredVertexIndexTriangle{ { { 1, 6, 2 }, { -1, 0, 0 } }, Colours::aquamarine },
            Raster::ColouredVertexIndexTriangle{ { { 4, 5, 1 }, { 0, 1, 0 } }, Colours::lime },
            Raster::ColouredVertexIndexTriangle{ { { 4, 1, 0 }, { 0, 1, 0 } }, Colours::chartreuse },
            Raster::ColouredVertexIndexTriangle{ { { 2, 6, 7 }, { 0, -1, 0 } }, Colours::yellow },
            Raster::ColouredVertexIndexTriangle{ { { 2, 7, 3 }, { 0, -1, 0 } }, Colours::orange },
        }
    };

    Raster::IndexModel sphere_index_model;
    const int v_divs = 10;
    const int h_divs = 10;
    for (double v_div = 0; v_div <= v_divs; ++v_div) {
        double theta = std::numbers::pi * (v_div / v_divs);
        for (double h_div = 0; h_div < h_divs; ++h_div) {
            double phi = 2 * std::numbers::pi * (h_div / h_divs);
            auto z = std::sin(theta) * std::cos(phi);
            auto x = std::sin(theta) * std::sin(phi);
            auto y = std::cos(theta);

            sphere_index_model.vertices.emplace_back(Homogeneous::RawPoint3D{ x, y, z });
        }
    }
    for (int v_div = 0; v_div < v_divs; ++v_div) {
        for (int h_div = 0; h_div < h_divs; ++h_div) {
            auto h_div_next = (h_div + 1) % h_divs;
            auto v_div_next = v_div + 1;

            Raster::VertexIndex vert00 = h_div + (v_div * h_divs);
            Raster::VertexIndex vert01 = h_div_next + (v_div * h_divs);
            Raster::VertexIndex vert02 = h_div + (v_div_next * h_divs);

            Raster::VertexIndex vert11 = h_div_next + (v_div * h_divs);
            Raster::VertexIndex vert10 = h_div_next + (v_div_next * h_divs);
            Raster::VertexIndex vert12 = h_div + (v_div_next * h_divs);

            sphere_index_model.triangles.push_back({
                { { vert00, vert01, vert02 }, {} },
                Colours::white
            });
            sphere_index_model.triangles.push_back({
                { { vert10, vert11, vert12 }, {} },
                Colours::red
            });
        }
    }

    Raster::Model cube_model(cube_index_model);
    Raster::Model sphere_model(sphere_index_model);

    Raster::Instance cube_inst1{ cube_model, Homogeneous::translation<3>({ 2, -2, 6 }) };
    Raster::Instance cube_inst2{ cube_model, Homogeneous::translation<3>({ -2, 2, 9 }) };
    Raster::Instance sphere_inst{ sphere_model, Homogeneous::translation<3>({ 0, -0.5, 7 }) * Homogeneous::rotation_3d(-0.5, 0, 0) };

    Raster::Scene scene{{ cube_inst1, cube_inst2, sphere_inst }};

    auto m_projection = my_viewport.m_3d_to_canvas();

    auto m_camera = my_camera.inv_matrix();

    scene.render(raster_canvas, m_projection, m_camera);

    raster_canvas.clamp();
    raster_canvas.write_out(raster_image);
    std::cout << "Writing out raster\n";
    generateBitmapImage(raster_image.data(), c_w, c_h, "out/raster.bmp");
}

void generateBitmapImage (unsigned char* image, int height, int width, const char* imageFileName)
{
    int widthInBytes = width * BYTES_PER_PIXEL;

    unsigned char padding[3] = {0, 0, 0};
    int paddingSize = (4 - (widthInBytes) % 4) % 4;

    int stride = (widthInBytes) + paddingSize;

    FILE* imageFile = fopen(imageFileName, "wb");

    unsigned char* fileHeader = createBitmapFileHeader(height, stride);
    fwrite(fileHeader, 1, FILE_HEADER_SIZE, imageFile);

    unsigned char* infoHeader = createBitmapInfoHeader(height, width);
    fwrite(infoHeader, 1, INFO_HEADER_SIZE, imageFile);

    int i;
    for (i = 0; i < height; i++) {
        fwrite(image + (i*widthInBytes), BYTES_PER_PIXEL, width, imageFile);
        fwrite(padding, 1, paddingSize, imageFile);
    }

    fclose(imageFile);
}

unsigned char* createBitmapFileHeader (int height, int stride)
{
    int fileSize = FILE_HEADER_SIZE + INFO_HEADER_SIZE + (stride * height);

    static unsigned char fileHeader[] = {
        0,0,     /// signature
        0,0,0,0, /// image file size in bytes
        0,0,0,0, /// reserved
        0,0,0,0, /// start of pixel array
    };

    fileHeader[ 0] = (unsigned char)('B');
    fileHeader[ 1] = (unsigned char)('M');
    fileHeader[ 2] = (unsigned char)(fileSize      );
    fileHeader[ 3] = (unsigned char)(fileSize >>  8);
    fileHeader[ 4] = (unsigned char)(fileSize >> 16);
    fileHeader[ 5] = (unsigned char)(fileSize >> 24);
    fileHeader[10] = (unsigned char)(FILE_HEADER_SIZE + INFO_HEADER_SIZE);

    return fileHeader;
}

unsigned char* createBitmapInfoHeader (int height, int width)
{
    static unsigned char infoHeader[] = {
        0,0,0,0, /// header size
        0,0,0,0, /// image width
        0,0,0,0, /// image height
        0,0,     /// number of color planes
        0,0,     /// bits per pixel
        0,0,0,0, /// compression
        0,0,0,0, /// image size
        0,0,0,0, /// horizontal resolution
        0,0,0,0, /// vertical resolution
        0,0,0,0, /// colors in color table
        0,0,0,0, /// important color count
    };

    infoHeader[ 0] = (unsigned char)(INFO_HEADER_SIZE);
    infoHeader[ 4] = (unsigned char)(width      );
    infoHeader[ 5] = (unsigned char)(width >>  8);
    infoHeader[ 6] = (unsigned char)(width >> 16);
    infoHeader[ 7] = (unsigned char)(width >> 24);
    infoHeader[ 8] = (unsigned char)(height      );
    infoHeader[ 9] = (unsigned char)(height >>  8);
    infoHeader[10] = (unsigned char)(height >> 16);
    infoHeader[11] = (unsigned char)(height >> 24);
    infoHeader[12] = (unsigned char)(1);
    infoHeader[14] = (unsigned char)(BYTES_PER_PIXEL*8);

    return infoHeader;
}
