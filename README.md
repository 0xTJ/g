G
=

A header-only C++ library for graphics.

Requires C++20 to compile.

Results
-

### Raycast

![Raycast Demo](out/raycast.bmp)

### Raster

![Raster Demo](out/raster.bmp)
